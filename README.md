# Pimcore Docker Compose Environment

## Requirements

This setup has been tested to work with Pimcore 5.4.2.

## Install / Setup

Use composer on your *host* machine to download the
required pimcore release - for PACKAGE choose between:

* skeleton
* demo-basic
* demo-basic-twig
* demo-ecommerce

Command:

    COMPOSER_MEMORY_LIMIT=3G php -d memory_limit=-1 `which composer` create-project pimcore/PACKAGE project

Example demo:

    COMPOSER_MEMORY_LIMIT=3G php -d memory_limit=-1 `which composer` create-project pimcore/demo-basic-twig project

Example empty (skeleton):

    COMPOSER_MEMORY_LIMIT=3G php -d memory_limit=-1 `which composer` create-project pimcore/skeleton project

Then run ```docker-compose up``` and keep that process running.

Wait (on first start) until pimcore installation is finished - it
shows something like this once it is completed (after ~5 minutes) 
and the system is usable:

        Creating network "pimcore_default" with the default driver
        Creating volume "pimcore_pimcore-mysqldb" with default driver
        Creating pimcore_redis_1 ... done
        Creating pimcore_mysql_1 ... done
        Creating pimcore_php-fpm_1 ... done
        Creating pimcore_nginx_1   ... done
        Attaching to pimcore_redis_1, pimcore_mysql_1, pimcore_php-fpm_1, pimcore_nginx_1
        redis_1    | WARNING: no logs are available with the 'none' log driver
        mysql_1    | WARNING: no logs are available with the 'none' log driver
        nginx_1    | WARNING: no logs are available with the 'none' log driver
        php-fpm_1  | Waiting for MySQL on host [mysql] port [3306]:
        php-fpm_1  | Still waiting...
        php-fpm_1  | Still waiting...
        php-fpm_1  | Still waiting...
        php-fpm_1  | Still waiting...
        php-fpm_1  | OK - MySQL Server is up!
        php-fpm_1  | Changing owner of all files in /php to www-data ... OK.
        php-fpm_1  | Running Pimcore install command [/php/bin/install pimcore:install --no-interaction] ...
        php-fpm_1  |
        php-fpm_1  | Running installation. You can find a detailed install log in var/installer/logs/prod.log
        php-fpm_1  |
        php-fpm_1  | Starting the install process...
        php-fpm_1  |
        php-fpm_1  |   0/11 [>---------------------------]   0%
        php-fpm_1  |
        [..]
        php-fpm_1  | Installing bundles...
        php-fpm_1  |
        php-fpm_1  |  11/11 [============================] 100%
        php-fpm_1  |
        php-fpm_1  |  Trying to install assets as relative symbolic links.
        php-fpm_1  |
        php-fpm_1  |  [OK] No assets were provided by any bundle.
        php-fpm_1  |
        php-fpm_1  |  [OK] Pimcore was successfully installed
        php-fpm_1  |
        php-fpm_1  | OK - php-fpm-pimcore startup complete.
        php-fpm_1  | Finally starting php-fpm:
        php-fpm_1  | [20-Jun-2018 14:02:46] NOTICE: fpm is running, pid 36
        php-fpm_1  | [20-Jun-2018 14:02:46] NOTICE: ready to handle connections

Finally, Pimcore is available at :[http://localhost:8080/admin](http://localhost:8080/admin) -
the default admin account is set up with the following credentials:
```
Username: admin
Password: admin
```

*If you run into errors/exceptions when trying to open/reach Pimcore, you might need to do the following:*
```
docker-compose exec php-fpm su -s /bin/bash www-data
cd /php
./vendor/bin/pimcore-install --ignore-existing-config
```

The first start is going to be slow, as Pimcore compiles css / js on
the initial request.

You can stop the containers with CTRL-C and start them later on again
with ```docker-compose up``` - please ignore the error after startup,
this is just due to the installer being run an detecting an already
installed system.

## How to ...

### Shell access

To get a shell within the php-fpm container (in order to execute scripts), 
you can run: 

    docker-compose exec php-fpm su -s /bin/bash www-data

### Purge the system (aka delete everything, start all over)

Stop the containers and run ```docker-compose down -v```; afterwards delete 
the ```/project``` directory.

## Misc. / References

* Pimcore Documentation: [https://pimcore.com/docs/5.x/index.html](https://pimcore.com/docs/5.x/index.html)
